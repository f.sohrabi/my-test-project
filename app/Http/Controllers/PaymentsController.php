<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentsRequest;

use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class PaymentsController extends Controller
{


    public function payment(PaymentsRequest $request)
    {

        try {
            $donait = sprintf("%.2f", $request->donait);;
            $payment = Mollie::api()->payments()->create([
                'amount' => [
                    'currency' => 'EUR',
                    'value' => $donait, // You must send the correct number of decimals, thus we enforce the use of strings
                ],
                'description' => 'My first API payment',

                'redirectUrl' => route('order.result'),
                "method" => 'creditcard'
            ]);
            $request->session()->put('paymentId', $payment->id);
            $payment = Mollie::api()->payments()->get($payment->id);

            // redirect customer to Mollie checkout page
            return redirect($payment->getCheckoutUrl(), 303);

        } catch (\Mollie\Api\Exceptions\ApiException $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());
        }
    }

    public function resultPayment(Request $request)
    {
        $payment = Mollie::api()->payments()->get($request->session()->get('paymentId'));
        return view('result', compact('payment'));

    }


}
